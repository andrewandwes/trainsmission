﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wrench : MonoBehaviour {

	public Vector3 direction;
	public Vector3 additionalMomentum;
	public float rotation = 0.00000001f;
	static float timeToLive = 2.0f;
	public float timeAlive = 0.0f;
	public bool alive = false;

	// Use this for initialization
	void Start () {
		timeAlive = 0.0f;
		transform.rotation = new Quaternion(transform.rotation.x,transform.rotation.y, -1.0f, transform.rotation.w);
	}
	
	// Update is called once per frame
	void Update () {
		if (alive) {
			transform.position = transform.position + direction + additionalMomentum;
			if (transform.rotation.z >= 0.98f) {
				transform.rotation = new Quaternion(transform.rotation.x,transform.rotation.y, -1.0f, transform.rotation.w);
			}
			transform.rotation = new Quaternion(transform.rotation.x,transform.rotation.y, transform.rotation.z + rotation, transform.rotation.w);
			if (timeAlive > timeToLive) {
				DestroyObject (gameObject);
			}
			timeAlive = timeAlive + Time.deltaTime;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag.Equals ("Enemy")) {
			var script = (enemy)(other.gameObject).GetComponent (typeof(enemy));
			script.EnemyHit();
//			other.gameObject.SetActive (false);
			DestroyObject (gameObject);
		} else if (!other.gameObject.tag.Equals ("Player")) {
			DestroyObject (gameObject);
		}
	}
		
}
