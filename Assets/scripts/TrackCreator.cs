﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackCreator : MonoBehaviour {
	
	public Transform brick;

	// Use this for initialization
	void Start () {
		var path = new List<Vector2> ();
		path.Add (new Vector2 (1,1));
		path.Add (new Vector2 (1,2));
		path.Add (new Vector2 (1,3));
		path.Add (new Vector2 (1,4));
		path.Add (new Vector2 (1,5));
		path.Add (new Vector2 (1,6));
		path.Add (new Vector2 (1,7));
		path.Add (new Vector2 (1,8));
		path.Add (new Vector2 (2,8));
		path.Add (new Vector2 (3,8));
		path.Add (new Vector2 (4,8));
		path.Add (new Vector2 (5,8));
		path.Add (new Vector2 (6,8));
		path.Add (new Vector2 (7,8));
		path.Add (new Vector2 (8,8));
		path.Add (new Vector2 (9,8));
		path.Add (new Vector2 (10,8));
		path.Add (new Vector2 (11,8));
		path.Add (new Vector2 (12,8));
		path.Add (new Vector2 (12,7));
		path.Add (new Vector2 (12,6));
		path.Add (new Vector2 (12,5));
		path.Add (new Vector2 (12,4));
		path.Add (new Vector2 (12,3));
		path.Add (new Vector2 (12,2));
		path.Add (new Vector2 (12,1));
		path.Add (new Vector2 (11,1));
		path.Add (new Vector2 (10,1));
		path.Add (new Vector2 (9,1));
		path.Add (new Vector2 (8,1));
		path.Add (new Vector2 (7,1));
		path.Add (new Vector2 (6,1));
		path.Add (new Vector2 (5,1));
		path.Add (new Vector2 (4,1));
		path.Add (new Vector2 (3,1));
		path.Add (new Vector2 (2,1));
		var tileWidth = 0.8f;
		var startX = -5.2f;
		var startY = 3.6f;
		foreach (Vector2 location in path) {
			var newX = startX + (tileWidth * location.x);
			var newY = startY - (tileWidth * location.y);
			Instantiate (brick, new Vector3 (newX, newY, 0), Quaternion.identity);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
