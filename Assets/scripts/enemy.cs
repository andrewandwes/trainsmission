﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour {

	public GameObject conductor;
	public Sprite[] sprites;
	public int strength = 1;


	//NOT USED UNTIL LATER REFACTORING

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.MoveTowards (transform.position, conductor.transform.position, 0.01f);
		RotateEnemyIntoMoveDirection ();
	}

	private void RotateEnemyIntoMoveDirection() {
		Vector3 newDirection = (transform.position - conductor.transform.position);
		//2
		float x = newDirection.x;
		float y = newDirection.y;
		float rotationAngle = Mathf.Atan2(x, y) * 180 / Mathf.PI;
		transform.rotation = Quaternion.AngleAxis(rotationAngle, Vector3.back);
	}

	public void EnemyHit() {
		strength--;

		switch (strength)
		{
		case 1:
			this.GetComponent<SpriteRenderer>().sprite = sprites[0];
			break;
		case 2:
			this.GetComponent<SpriteRenderer>().sprite = sprites[1];
			break;
		default:
			Destroy (gameObject);
			break;
		}
	}
//
//	private void enemyUpdate() {
//		transform.position = Vector3.MoveTowards (transform.position, conductor.transform.position, 0.01f);
//		RotateEnemyIntoMoveDirection ();
//
//	}
//
//	private void RotateEnemyIntoMoveDirection(){
//		Vector3 newDirection = (transform.position - conductor.transform.position);
//		//2
//		float x = newDirection.x;
//		float y = newDirection.y;
//		float rotationAngle = Mathf.Atan2(x, y) * 180 / Mathf.PI;
//		transform.rotation = Quaternion.AngleAxis(rotationAngle, Vector3.back);
//	}
}
