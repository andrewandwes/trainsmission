﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class train : MonoBehaviour {

	public GameObject[] waypoints;
	private int currentWaypoint = 0;
	private float lastWaypointSwitchTime;
	public float speed = 1f;
	public Vector3 initialPosition;

	// Use this for initialization
	void Start () {
		lastWaypointSwitchTime = Time.time;
	}
	
	// Update is called once per frame
	void Update()
	{
		speed += Time.timeSinceLevelLoad / 8000.0f;
		// 1 
		Vector3 startPosition = waypoints[currentWaypoint].transform.position;
		int endWaypoint = currentWaypoint + 1;
		if (endWaypoint > waypoints.Length - 1) {
			endWaypoint = 0;
		}
		Vector3 endPosition = waypoints[endWaypoint].transform.position;
		// 2 
		float pathLength = Vector2.Distance(startPosition, endPosition);
		float totalTimeForPath = pathLength / speed;
		float currentTimeOnPath = Time.time - lastWaypointSwitchTime;
		gameObject.transform.position = Vector2.Lerp(startPosition, endPosition, currentTimeOnPath / totalTimeForPath);
		// 3 
		if (gameObject.transform.position.Equals(endPosition))
		{
			if (currentWaypoint < waypoints.Length - 1)
			{
				// 4 Switch to next waypoint
				currentWaypoint++;
				lastWaypointSwitchTime = Time.time;

				RotateIntoMoveDirection();
			}
			else
			{
				currentWaypoint = 0;
				gameObject.transform.position = initialPosition;

			}
		}
	}

	private void RotateIntoMoveDirection()
	{
		int endWaypoint = currentWaypoint + 1;
		if (endWaypoint > waypoints.Length - 1) {
			endWaypoint = 0;
		}
		//1
		Vector3 newStartPosition = waypoints[currentWaypoint].transform.position;
		Vector3 newEndPosition = waypoints[endWaypoint].transform.position;
		Vector3 newDirection = (newEndPosition - newStartPosition);
		//2
		float x = newDirection.x;
		float y = newDirection.y;
		float rotationAngle = Mathf.Atan2(y, x) * 180 / Mathf.PI;
		gameObject.transform.rotation = Quaternion.AngleAxis(rotationAngle, Vector3.forward);
	}

	public float DistanceToGoal()
	{
		float distance = 0;
		distance += Vector2.Distance(
			gameObject.transform.position,
			waypoints[currentWaypoint + 1].transform.position);
		for (int i = currentWaypoint + 1; i < waypoints.Length - 1; i++)
		{
			Vector3 startPosition = waypoints[i].transform.position;
			Vector3 endPosition = waypoints[i + 1].transform.position;
			distance += Vector2.Distance(startPosition, endPosition);
		}
		return distance;
	}
}
