﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnEnemies : MonoBehaviour {

	public GameObject[] spawnPoints;
	public GameObject[] enemies;
	private float spawnTime = 1.0f;
	public float currentTime = 0.0f;
	private float totalTime = 0.0f;

	// Use this for initialization
	void Start () {
		generateEnemy();
	}
	
	// Update is called once per frame
	void Update () {
		totalTime += Time.deltaTime;
		spawnTime -= totalTime / 100000.0f;
		currentTime += Time.deltaTime;
		if (currentTime > spawnTime) {
			currentTime = 0.0f;
			generateEnemy();
		}
	}

	private void generateEnemy() {
		GameObject enemy = enemies [Random.Range (0, enemies.Length)];
		GameObject point = spawnPoints [Random.Range (0, spawnPoints.Length)];
		var newEnemy = (GameObject)Instantiate (enemy, point.transform.position, point.transform.rotation);
	}
		
}
