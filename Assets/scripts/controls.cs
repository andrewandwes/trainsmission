﻿using UnityEngine;
using System.Collections;

public class controls : MonoBehaviour {
	public float MovementSpeed = 0.04f;
	public float FireSpeed = 0.04f;
	public GameObject weapon;
	public GameObject[] existingBullets;
	static float fireRate = 0.3f;
	float timeToNextBullet = 0.0f;
	private Vector3 movementOffset;
	private int playerHealth = 1;

	void Start () {
	}

	// Update is called once per frame
	void Update () {
		
		move();
		fire();
	}

	void move() {
		float Xon = Input.GetAxis ("Horizontal") * MovementSpeed;
		float Yon = Input.GetAxis ("Vertical") * MovementSpeed;
		if (Input.GetKey (KeyCode.W)) {
			Yon = MovementSpeed;
		} else if (Input.GetKey (KeyCode.S)) {
			Yon =  - MovementSpeed;
		}
		if (Input.GetKey (KeyCode.A)) {
			Xon = -MovementSpeed;
		} else if (Input.GetKey (KeyCode.D)) {
			Xon = MovementSpeed;
		}
		Vector3 offset = Vector3.zero;
		movementOffset = new Vector3 (offset.x += Xon, offset.y += Yon, offset.z);

		gameObject.transform.position = transform.position + movementOffset;
	}

	void fire() {
		float Xon = Input.GetAxis ("Horizontal2") * MovementSpeed;
		float Yon = Input.GetAxis ("Vertical2") * MovementSpeed;
		if (Input.GetKey (KeyCode.I)) {
			Yon = MovementSpeed;
		} else if (Input.GetKey (KeyCode.K)) {
			Yon =  - MovementSpeed;
		}
		if (Input.GetKey (KeyCode.J)) {
			Xon = -MovementSpeed;
		} else if (Input.GetKey (KeyCode.L)) {
			Xon = MovementSpeed;
		}
		Vector3 offset = Vector3.zero;
		offset = new Vector3 (offset.x += Xon, offset.y += Yon, offset.z);

		if (0.0f != Xon || Yon != 0.0f) {
			RotateConductorIntoMoveDirection (transform.position + offset);
			if (timeToNextBullet <= 0.0f) {
				createWrench (offset);
			}
		}

		if (Input.GetButtonDown ("Fire1") || Input.GetKey(KeyCode.LeftShift)) {
			crazyFire ();
		}

		timeToNextBullet -= Time.deltaTime;

	}

	void crazyFire() {
		for (int i = 0; i < 25; i++) {
			switch (Random.Range (0, 4))
			{
			case 0:
				createWrench (new Vector3 (-Random.value, -Random.value));
				break;
			case 1:
				createWrench(new Vector3(Random.value,Random.value));
				break;
			case 2:
				createWrench(new Vector3(-Random.value,Random.value));
				break;
			case 3:
				createWrench(new Vector3(Random.value,-Random.value));
				break;
			}
		}
	}

	void createWrench(Vector3 offset) {
		var bullet = (GameObject)Instantiate (weapon, transform.position, transform.rotation);
		var script = (wrench)bullet.GetComponent (typeof(wrench));
		script.additionalMomentum = movementOffset;
		script.direction = offset;
		script.alive = true;
		timeToNextBullet = fireRate;
	}

		

	private void RotateConductorIntoMoveDirection(Vector3 newPosition){
		Vector3 newDirection = (transform.position - newPosition);
		//2
		float x = newDirection.x;
		float y = newDirection.y;
		float rotationAngle = Mathf.Atan2(x, y) * 180 / Mathf.PI;
		transform.rotation = Quaternion.AngleAxis(rotationAngle, Vector3.back);
	}

	void OnTriggerEnter2D(Collider2D other) {
//		if (other.gameObject.tag.Equals ("Enemy") || other.gameObject.tag.Equals ("Train")) {
//			playerHealth--;			
//		}
//		if (playerHealth == 0) {
//			for (int i = 0; i < gameObject.scene.GetRootGameObjects ().Length; i++) {
//				GameObject go = gameObject.scene.GetRootGameObjects()[i];
//				Destroy(go);
//			}
//				
//		}
	}

}
